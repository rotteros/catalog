<?php

namespace LukaszGrabek\Product\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class SetWarehouseNameAttribute
 * @package LukaszGrabek\Product\Observer
 */
class SetWarehouseNameAttribute implements ObserverInterface
{
    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $quoteItem = $observer->getQuoteItem();
        $product = $observer->getProduct();
        $quoteItem->setWarehouseName($product->getCustomAttribute('warehouse_name')->getValue());
    }
}
