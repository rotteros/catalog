<?php

namespace LukaszGrabek\Product\Block;

use LukaszGrabek\Product\Block\WarehouseName;
use Magento\Framework\View\Element\Template\Context as Context;
use Magento\Framework\View\Element\Template as Template;

/**
 * Class CustomerWarehouseName
 * @package LukaszGrabek\Product\Block
 */
class CustomerWarehouseName extends Template
{
    /**
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
    }

    /**
     * @return void
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
    }

    /**
     * @return WarehouseName
     */
    public function getWarehouseName()
    {
        $_item = $this->getItem();
        $_product = $_item->getProduct();
        $data = $_item->getOrigData('warehouse_name');

        return $data;
    }
}
