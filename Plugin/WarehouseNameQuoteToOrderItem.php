<?php

namespace LukaszGrabek\Product\Plugin;

use Closure;
use Magento\Quote\Model\Quote\Item\AbstractItem as AbstractItem;
use Magento\Sales\Model\Order\Item;

/**
 * Class WarehouseNameQuoteToOrderItem
 * @package LukaszGrabek\Product\Plugin
 */
class WarehouseNameQuoteToOrderItem
{
    public function aroundConvert(
        Closure $proceed,
        AbstractItem $item,
        $additional = []
    ) {
        /** @var $orderItem Item */
        $orderItem = $proceed($item, $additional);
        $orderItem->setWarehouseName($item->getWarehouseName());

        return $orderItem;
    }
}
